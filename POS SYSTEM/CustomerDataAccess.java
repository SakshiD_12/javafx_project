import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class CustomerDataAccess {

    // JDBC URL, username, and password of MySQL server
    private static final String URL = "jdbc:mysql://localhost:3306/coffeeshop";
    private static final String USER = "root";
    private static final String PASSWORD = "root";

    // JDBC variables for managing the connection and executing SQL queries
    private static Connection connection;

    public static void insertCustomerData(String name, String mobile, String address, String orderDate, String orderTime) {
        try {
            // Establish a connection
            connection = DriverManager.getConnection(URL, USER, PASSWORD);

            // The SQL query to insert customer data
            String insertQuery = "INSERT INTO customers (name, mobile, address, order_date, order_time) VALUES (?, ?, ?, ?, ?)";

            // Create a prepared statement
            PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, mobile);
            preparedStatement.setString(3, address);
            preparedStatement.setString(4, orderDate);
            preparedStatement.setString(5, orderTime);

            // Execute the query
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
