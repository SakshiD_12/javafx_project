import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javafx.scene.control.Alert;
import javafx.stage.Stage;

public class DatabaseConnection {

    private static final String DB_URL = "jdbc:mysql://localhost:3306/coffeeshop";
    private static final String DB_USER = "root";
    private static final String DB_PASSWORD = "root";

    static LoginPage lp =new LoginPage();

    public void insertUser(String username, String password, Stage primaryStage) {
        try {
            Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            String query = "INSERT INTO adminlogin (username, password) VALUES (?, ?)";
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            preparedStatement.executeUpdate();

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setContentText("Account Created succesfully");
            alert.showAndWait();
            lp.goToLoginPage(primaryStage);

        
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
