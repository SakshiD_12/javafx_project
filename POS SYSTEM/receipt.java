import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class receipt extends Application {
    @Override
    public void start(Stage primaryStage) {
        StackPane root = new StackPane();
        root.setPrefSize(900.0, 900.0);

        AnchorPane anchorPane = new AnchorPane();
        anchorPane.setPrefSize(400.0, 400.0);

        ImageView topImageView = new ImageView();
        topImageView.setFitHeight(300.0);
        topImageView.setFitWidth(880.0);
        topImageView.setLayoutY(-6.0);
        topImageView.setPickOnBounds(true);
        topImageView.setImage(new Image("top.jpg"));

        Label customerNameLabel = new Label("Customer Name");
        customerNameLabel.setLayoutX(40.0);
        customerNameLabel.setLayoutY(330.0);
        Font customerNameFont = Font.font("System Bold", 25.0);
        customerNameLabel.setFont(customerNameFont);

        Label addressLabel = new Label("Address");
        addressLabel.setLayoutX(40.0);
        addressLabel.setLayoutY(390.0);
        Font addressFont = Font.font("System Bold", 25.0);
        addressLabel.setFont(addressFont);

        Label mobNoLabel = new Label("Mob No");
        mobNoLabel.setLayoutX(40.0);
        mobNoLabel.setLayoutY(450.0);
        Font mobNoFont = Font.font("System Bold", 25.0);
        mobNoLabel.setFont(mobNoFont);


        Label dateLabel = new Label("Date");
        dateLabel.setLayoutX(550.0);
        dateLabel.setLayoutY(330.0);
        Font dateFont = Font.font("System Bold", 25.0);
        dateLabel.setFont(dateFont);

        Label timeLabel = new Label("Time");
        timeLabel.setLayoutX(550.0);
        timeLabel.setLayoutY(390.0);
        Font timeFont = Font.font("System Bold", 25.0);
        timeLabel.setFont(timeFont);

        ImageView centreImageView = new ImageView();
        centreImageView.setFitHeight(344.0);
        centreImageView.setFitWidth(900.0);
        centreImageView.setLayoutX(11.0);
        centreImageView.setLayoutY(500.0);
        centreImageView.setOpacity(0.34);
        centreImageView.setPickOnBounds(true);
        centreImageView.setImage(new Image("centre.jpg"));

        TableView tableView = new TableView();
        tableView.setLayoutX(45.0);
        tableView.setLayoutY(520.0);
        tableView.setPrefHeight(286.0);
        tableView.setPrefWidth(820.0);

        TableColumn serialNoColumn = new TableColumn("Serial No");
        serialNoColumn.setPrefWidth(90.0);

        TableColumn productNameColumn = new TableColumn("Product Name");
        productNameColumn.setPrefWidth(250.0);

        TableColumn quantityColumn = new TableColumn("Quantity");
        quantityColumn.setPrefWidth(200.800048828125);

        TableColumn priceColumn = new TableColumn("Price");
        priceColumn.setPrefWidth(200.800048828125);

        tableView.getColumns().addAll(serialNoColumn, productNameColumn, quantityColumn, priceColumn);

        Label totalLabel = new Label("Total  :");
        totalLabel.setLayoutX(530.0);
        totalLabel.setLayoutY(850.0);
        Font totalFont = Font.font("System Bold", 25.0);
        totalLabel.setFont(totalFont);

        TextField customerNameTextField = new TextField();
        customerNameTextField.setLayoutX(250.0);
        customerNameTextField.setLayoutY(330.0);
        customerNameTextField.setPrefHeight(35.0);
        customerNameTextField.setPrefWidth(200.0);

        TextField addressTextField = new TextField();
        addressTextField.setLayoutX(250.0);
        addressTextField.setLayoutY(390.0);
        addressTextField.setPrefHeight(35.0);
        addressTextField.setPrefWidth(200.0);

        TextField mobNoTextField = new TextField();
        mobNoTextField.setLayoutX(250.0);
        mobNoTextField.setLayoutY(450.0);
        mobNoTextField.setPrefHeight(35.0);
        mobNoTextField.setPrefWidth(200.0);

        TextField dateTextField = new TextField();
        dateTextField.setLayoutX(650.0);
        dateTextField.setLayoutY(330.0);
        dateTextField.setPrefHeight(35.0);
        dateTextField.setPrefWidth(200.0);

        TextField timeTextField = new TextField();
        timeTextField.setLayoutX(650.0);
        timeTextField.setLayoutY(390.0);
        timeTextField.setPrefHeight(35.0);
        timeTextField.setPrefWidth(200.0);

        TextField totalTextField = new TextField();
        totalTextField.setLayoutX(630.0);
        totalTextField.setLayoutY(850.0);
        totalTextField.setPrefHeight(35.0);
        totalTextField.setPrefWidth(200.0);

        Text text = new Text("Cup N Convo");
        text.setFill(javafx.scene.paint.Color.WHITE);
        text.setFontSmoothingType(javafx.scene.text.FontSmoothingType.LCD);
        text.setLayoutX(250.0);
        text.setLayoutY(76.0);
        text.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        text.setStrokeWidth(0.0);
        text.setText("Cup N Convo");
        text.setWrappingWidth(401.48291015625);
        Font textFont = Font.font("Century", 63.0);
        text.setFont(textFont);

        anchorPane.getChildren().addAll(
            topImageView,
            customerNameLabel,
            addressLabel,
            dateLabel,
            mobNoLabel,
            timeLabel,
            centreImageView,
            tableView,
            totalLabel,
            customerNameTextField,
            addressTextField,
            dateTextField,
            mobNoTextField,
            timeTextField,
            totalTextField,
            text
        );

        root.getChildren().add(anchorPane);

        Scene scene = new Scene(root, 850, 950);
        primaryStage.setScene(scene);
        primaryStage.setTitle("JavaFX App");
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
