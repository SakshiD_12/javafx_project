
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.animation.TranslateTransition;
import javafx.collections.ObservableList;

public class LoginPage{

    //SecondPage sp = new SecondPage();
    DatabaseController dc = new DatabaseController();
    static SignUP su= new SignUP();
    static mainWindow m = new mainWindow();
    static CustomerD cu=new CustomerD();

    PasswordField passwordField = new PasswordField();
     TextField usernameField = new TextField();
     Button loginButton = new Button("LOGIN");
     Stage ps ;

    
    public void goToLoginPage(Stage primaryStage) {
        ps = primaryStage;
        StackPane root = new StackPane();

        AnchorPane anchorPane = new AnchorPane();
        anchorPane.setPrefSize(500, 400);
       // anchorPane.setStyle("-fx-background-color: #F5F7F8");

        AnchorPane innerAnchorPane = new AnchorPane();
        innerAnchorPane.setPrefSize(600, 400);

        ImageView imageView = new ImageView(new Image("18.jpg"));
        imageView.setFitWidth(1200);
        imageView.setFitHeight(955);
        imageView.setLayoutX(-1);
        imageView.setLayoutY(-4);
        imageView.setPickOnBounds(true);

        ImageView secondImage = new ImageView(new Image("18.jpg"));
        secondImage.setFitWidth(750);
        secondImage.setFitHeight(955);
        secondImage.setLayoutX(1200);
        secondImage.setLayoutY(-4);
        secondImage.setOpacity(0.3); // Adjust opacity as needed

       // TextField usernameField = new TextField();
        usernameField.setLayoutX(1370);
        usernameField.setLayoutY(400);
        usernameField.setPrefWidth(400);
        usernameField.setPrefHeight(50);
        usernameField.setPromptText("USERNAME");
        usernameField.setStyle("-fx-background-radius: 28;");


       // PasswordField passwordField = new PasswordField();
        passwordField.setLayoutX(1370);
        passwordField.setLayoutY(490);
        passwordField.setPrefWidth(400);
        passwordField.setPrefHeight(50);
        passwordField.setPromptText("PASSWORD");
          passwordField.setStyle("-fx-background-radius: 28;");
          
        // Button loginButton = new Button("LOGIN");
        loginButton.setLayoutX(1370);
        loginButton.setLayoutY(630);
        loginButton.setPrefWidth(400);
        loginButton.setPrefHeight(50);
        loginButton.setStyle("-fx-font-weight: bold; -fx-font-size: 25px;  -fx-background-radius: 28;");
        loginButton.setMnemonicParsing(false);
        loginButton.setTextFill(Color.WHITE);


        loginButton.getStyleClass().add("button-login");
        loginButton.getStyleClass().add("button-login:hover");



        Button backButton = new Button("Back");
        backButton.setLayoutX(1800);
        backButton.setLayoutY(800);
        backButton.setPrefSize(100, 40);
        backButton.setStyle(
            "-fx-background-color: black; " + // Button background color
            "-fx-text-fill: white; " + // Button text color
            "-fx-font-size: 20px; " + // Button text size
            "-fx-border-radius: 50px;" +
            "-fx-background-radius: 28;"
        );

        Label titleLabel = new Label("Login Account");
        titleLabel.setLayoutX(1300);
        titleLabel.setLayoutY(-10); // Start off the screen above
        titleLabel.setPrefWidth(500);
        titleLabel.setPrefHeight(90);
        titleLabel.setFont(new Font(55.0));
        titleLabel.setStyle("-fx-font-family: 'Times New Roman';");

        titleLabel.getStyleClass().add("label-title");

        // Forgot Password link
        Hyperlink forgotPasswordLink = new Hyperlink("Don't have account?");
        forgotPasswordLink.setLayoutX(1370);
        forgotPasswordLink.setLayoutY(560);
        forgotPasswordLink.setTextFill(Color.BLACK);
        forgotPasswordLink.setFont(new Font(25));

        innerAnchorPane.getChildren().addAll(imageView, secondImage,usernameField, passwordField, loginButton, titleLabel, forgotPasswordLink,backButton);
        anchorPane.getChildren().add(innerAnchorPane);
        root.getChildren().add(anchorPane);

        Scene scene = new Scene(root, 1900, 950);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Login App");
        primaryStage.show();
        primaryStage.setResizable(false);
        scene.getStylesheets().add("Styles.css");

        // Create a transition for the "Login Account" label
        TranslateTransition labelTransition = new TranslateTransition(Duration.seconds(2), titleLabel);
        labelTransition.setToY(250); // Move it to its original position

        // Play the label transition
        labelTransition.play();

        //loginButton.setOnAction(e -> sp.goTosecondPage(primaryStage,usernameField.getText(),passwordField.getText()));
        loginButton.setOnAction(e -> login());

        forgotPasswordLink.setOnAction(e-> su.goToSignUpPage(primaryStage));

        backButton.setOnAction(e ->m.start(primaryStage));

        
    }


    private void login() {
        dc.connection();

        //System.out.println(dc.getAdminName());
        // System.out.println(userName);
        // System.out.println(Password);

        ObservableList<String> user = dc.getAdminName();
        ObservableList<String> pass = dc.getPass();

        java.util.Iterator<String> itrUser = user.iterator();
        java.util.Iterator<String> itrPass = pass.iterator();

        int flag=0;
        while(itrUser.hasNext()){

            String uName = itrUser.next();
            String passName = itrPass.next();

            if(uName.equals(usernameField.getText()) && passName.equals(passwordField.getText())){
                flag=1;
                cu.goToCustomerPage(ps);
                break;
            }

        }

        if(flag ==0){
            Alert al = new Alert(Alert.AlertType.ERROR);
            al.setContentText("Invalid username or password");
            al.show();
        }
    }

        

}


