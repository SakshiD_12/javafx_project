import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.shape.Rectangle;


public class MenuPage{
    private static final String STYLESHEET_CASPIAN = null;
    private static final String STYLESHEET_MODENA = null;
    private TextArea itemTextArea;
    private TextArea quantityTextArea;

    //receipt r = new receipt();

    private TextArea priceTextArea;
    private TextArea totalArea;
    private Map<String, Integer> itemCounts = new HashMap<>();
    private double total = 0.0;  // Store the total price

    public void goToMenuPage(Stage primaryStage) {
        primaryStage.setTitle("Menu Page");

        VBox root = new VBox();
        root.setAlignment(Pos.TOP_CENTER);

        // Create a background image
        Image backgroundImage = new Image("items/background5.jpg");
        BackgroundImage background = new BackgroundImage(
                backgroundImage,
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                new BackgroundSize(2000, 1000, false, false, true, true));

        // Apply the background to the root VBox

        root.setBackground(new Background(background));

        ScrollPane scrollPane1 = new ScrollPane();
        scrollPane1.setContent(createMenuButtons()); // Add the GridPane to the ScrollPane
        scrollPane1.setFitToWidth(true);
        scrollPane1.setFitToHeight(true);

        scrollPane1.setStyle("0");
        scrollPane1.setPrefHeight(1000);
        scrollPane1.setMaxSize(2500, 1500);
        scrollPane1.setHbarPolicy(ScrollBarPolicy.NEVER);
        scrollPane1.setVbarPolicy(ScrollBarPolicy.NEVER);
        

        /***************** labels **********/
        BackgroundFill backgroundFill = new BackgroundFill(Color.LIGHTGRAY, null, null);
        Background ba = new Background(backgroundFill);

        Label label1 = new Label("   Item");
        label1.setStyle("-fx-font-size: 18; -fx-text-fill: white; -fx-alignment: center;");
        Label label2 = new Label("  Qty");
        label2.setStyle("-fx-font-size: 18; -fx-text-fill: white; -fx-alignment: center;");
        Label label3 = new Label("   Price");
        label3.setStyle("-fx-font-size: 18; -fx-text-fill: white; -fx-alignment: center;");
        Label label4 = new Label("             Total");
        label4.setBackground(ba);
        label4.setMinSize(255,100);
        label4.setFont(Font.font(STYLESHEET_MODENA, FontWeight.BOLD, null,25));
        Label label5 = new Label("      :   ");
        label5.setBackground(ba);
        label5.setMinSize(110,100);
        label5.setFont(Font.font(STYLESHEET_CASPIAN, FontWeight.BOLD, null,30));

        /********************* add labels****** */
        itemTextArea = new TextArea();
        itemTextArea.setEditable(false);
        itemTextArea.setWrapText(false);
        itemTextArea.setFont(Font.font("Arial", FontWeight.BOLD, 20));
       // itemTextArea.setVbarPolicy(ScrollBarPolicy.NEVER);


        quantityTextArea = new TextArea();
        quantityTextArea.setEditable(false);
        quantityTextArea.setWrapText(true);
        quantityTextArea.setFont(Font.font("Arial", FontWeight.BOLD, 20));


        priceTextArea = new TextArea();
        priceTextArea.setEditable(false);
        priceTextArea.setWrapText(true);
        priceTextArea.setFont(Font.font("Arial", FontWeight.BOLD, 20));

        
        totalArea = new TextArea();
        totalArea.setEditable(false);
        totalArea.setWrapText(true);
        totalArea.setFont(Font.font(STYLESHEET_CASPIAN, FontWeight.BOLD,  25));
        

        /************************ height for textareas********************** */
        itemTextArea.setPrefHeight(600);
        quantityTextArea.setPrefHeight(600);
        priceTextArea.setPrefHeight(600);
        totalArea.setPrefHeight(100);


        /***************************** width for textareas******************* */
        itemTextArea.setPrefWidth(255);
        quantityTextArea.setPrefWidth(110);
        priceTextArea.setPrefWidth(150);
        totalArea.setPrefWidth(150);

        /********** grid ******************* */
        GridPane textAreasGrid = new GridPane();
        textAreasGrid.setHgap(1); // Adjust the horizontal gap as needed
        textAreasGrid.setOpacity(1);
        textAreasGrid.add(label1, 0, 0);
        textAreasGrid.add(label2, 1, 0);
        textAreasGrid.add(label3, 2, 0);

        textAreasGrid.add(itemTextArea, 0, 1);
        textAreasGrid.add(quantityTextArea, 1, 1);
        textAreasGrid.add(priceTextArea, 2, 1);
        textAreasGrid.add(label4,0,2);
        textAreasGrid.add(label5, 1, 2);
        textAreasGrid.add(totalArea,2,2);

        // Create a VBox to hold scrollPane2 and the GridPane with text areas
        VBox rightContent = new VBox();
        /*************** Buy button************ */
        Image buy = new Image("items/buy.png");
        ImageView buypng = createRoundedCornerImageView(buy,340,110);
        Button buyButton = new Button("");
        buyButton.setPrefWidth(520);
        buyButton.setPrefHeight(140);
        buyButton.setGraphic(buypng);
       // buyButton.setOnAction(event ->r.setData(itemTextArea,quantityTextArea,priceTextArea,totalArea,primaryStage) );

        /************************* remove last button ******************* */
        Image Rl = new Image("items/Remove.png");
        ImageView Rlpng = createRoundedCornerImageView(Rl,340,110);
        Button removeLastButton = new Button("");
        removeLastButton.setPrefWidth(520);
        removeLastButton.setPrefHeight(140);
        removeLastButton.setGraphic(Rlpng);
        removeLastButton.setOnAction(event -> handleRemoveLastButtonClick());

        rightContent.getChildren().addAll(textAreasGrid, removeLastButton, buyButton);

        // Modify the layout of `righBox` to include `scrollPane1` and `rightContent`
        HBox rightBox = new HBox();
        rightBox.getChildren().addAll(scrollPane1, rightContent);
        rightBox.setOpacity(0.8);

        // Add the HBox to the root
        root.getChildren().add(rightBox);

        /*************************************************************************************************** */

        Scene scene = new Scene(root, 2000, 1000); // Adjust the scene size as needed

        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private GridPane createMenuButtons() {
        GridPane gridPane = new GridPane();
        gridPane.setOpacity(1);
        gridPane.setHgap(20);
        gridPane.setVgap(20);
        gridPane.setPadding(new Insets(20, 20, 20, 20));

        Image coffeepng = new Image("items/coffee.png");
        Image coffeeImage1 = new Image("items/Americano.jpg");
        Image coffeeImage2 = new Image("items/cappuccino1.jpg");
        Image coffeeImage3 = new Image("items/expresso.jpg");
        Image coffeeImage4 = new Image("items/iced_coffee.jpg");
        Image coffeeImage5 = new Image("items/latte.jpeg");
        Image coffeeImage6 = new Image("items/Macchiato.jpg");
        Image snackspng = new Image("items/snacks.png");
        Image pizzaImage1 = new Image("items/margarita.jpg");
        Image pizzaImage2 = new Image("items/pepperoni.jpg");
        Image pizzaImage3 = new Image("items/panner.jpg");
        Image pizzaImage4 = new Image("items/doublechees.jpg");
        Image burgerImage1 = new Image("items/vegburger.jpg");
        Image burgerImage2 = new Image("items/chickenburger.jpg");
        Image burgerImage3 = new Image("items/cheseburger.jpg");
        Image burgerImage4 = new Image("items/doublecheseburger.jpg");
        Image sandwichImage1 = new Image("items/sandwich.jpg");
        Image sandwichImage2 = new Image("items/grilledsandwich.jpg");
        Image friesImage = new Image("items/fries.jpg");
        Image drinkpng = new Image("items/drinks.png");
        Image drinkImage1 = new Image("items/coca-cola2.jpg");
        Image drinkImage2 = new Image("items/pepsi.jpg");
        Image drinkImage3 = new Image("items/sprite.jpg");

        double imageWidth = 420;
        double imageHeight = 320;

        ImageView cpng = createRoundedCornerImageView(coffeepng,imageWidth,imageHeight);
        ImageView coffee1 = createRoundedCornerImageView(coffeeImage1, imageWidth, imageHeight);
        ImageView coffee2 = createRoundedCornerImageView(coffeeImage2, imageWidth, imageHeight);
        ImageView coffee3 = createRoundedCornerImageView(coffeeImage3, imageWidth, imageHeight);
        ImageView coffee4 = createRoundedCornerImageView(coffeeImage4, imageWidth, imageHeight);
        ImageView coffee5 = createRoundedCornerImageView(coffeeImage5, imageWidth, imageHeight);
        ImageView coffee6 = createRoundedCornerImageView(coffeeImage6, imageWidth, imageHeight);
        ImageView spng = createRoundedCornerImageView(snackspng, imageWidth,imageHeight);
        ImageView pizza1 = createRoundedCornerImageView(pizzaImage1, imageWidth, imageHeight);
        ImageView pizza2 = createRoundedCornerImageView(pizzaImage2, imageWidth, imageHeight);
        ImageView pizza3 = createRoundedCornerImageView(pizzaImage3, imageWidth, imageHeight);
        ImageView pizza4 = createRoundedCornerImageView(pizzaImage4, imageWidth, imageHeight);
        ImageView burger1 = createRoundedCornerImageView(burgerImage1, imageWidth, imageHeight);
        ImageView burger2 = createRoundedCornerImageView(burgerImage2, imageWidth, imageHeight);
        ImageView burger3 = createRoundedCornerImageView(burgerImage3, imageWidth, imageHeight);
        ImageView burger4 = createRoundedCornerImageView(burgerImage4, imageWidth, imageHeight);
        ImageView sandwich1 = createRoundedCornerImageView(sandwichImage1, imageWidth, imageHeight);
        ImageView sandwich2 = createRoundedCornerImageView(sandwichImage2, imageWidth, imageHeight);
        ImageView fries = createRoundedCornerImageView(friesImage, imageWidth, imageHeight);
        ImageView dpng = createRoundedCornerImageView(drinkpng, imageWidth,imageHeight);
        ImageView drink1 = createRoundedCornerImageView(drinkImage1, imageWidth, imageHeight);
        ImageView drink2 = createRoundedCornerImageView(drinkImage2, imageWidth, imageHeight);
        ImageView drink3 = createRoundedCornerImageView(drinkImage3, imageWidth, imageHeight);

        // Add menu items (buttons) to the grid

        Button c1 = createMenuButton(coffee1, "Americano Coffee");
        Button c2 = createMenuButton(coffee2, "Cappuccino Coffee");
        Button c3 = createMenuButton(coffee3, "Expresso Coffee");
        Button c4 = createMenuButton(coffee4, "Iced Coffee");
        Button c5 = createMenuButton(coffee5, "Latte Coffee");
        Button c6 = createMenuButton(coffee6, "Macchiato Coffee");
        Button p1 = createMenuButton(pizza1, "Margarita Pizza");
        Button p2 = createMenuButton(pizza2, "Pepperoni Pizza");
        Button p3 = createMenuButton(pizza3, "Panner Pizza");
        Button p4 = createMenuButton(pizza4, "Doublechees Pizza");
        Button b1 = createMenuButton(burger1, "Veg Burger");
        Button b2 = createMenuButton(burger2, "Chicken Burger");
        Button b3 = createMenuButton(burger3, "Chese Burger");
        Button b4 = createMenuButton(burger4, "CDoublechese Burger");
        Button s1 = createMenuButton(sandwich1, "Sandwich");
        Button s2 = createMenuButton(sandwich2, "Grilled Sandwich");
        Button f = createMenuButton(fries, "Fries");
        Button d1 = createMenuButton(drink1, "Coca");
        Button d2 = createMenuButton(drink2, "Pepsi");
        Button d3 = createMenuButton(drink3, "Sprite");

        // Add items to the grid at specific row and column positions
        gridPane.add(cpng, 1, 0);
        gridPane.add(c1, 0, 1);
        gridPane.add(c2, 1, 1);
        gridPane.add(c3, 2, 1);
        gridPane.add(c4, 0, 2);
        gridPane.add(c5, 1, 2);
        gridPane.add(c6, 2, 2);
        gridPane.add(spng, 1, 3);
        gridPane.add(p1, 0, 4);
        gridPane.add(p2, 1, 4);
        gridPane.add(p3, 2, 4);
        gridPane.add(p4, 0, 5);
        gridPane.add(b1, 1, 5);
        gridPane.add(b2, 2, 5);
        gridPane.add(b3, 0, 6);
        gridPane.add(b4, 1, 6);
        gridPane.add(s1, 2, 6);
        gridPane.add(s2, 0, 7);
        gridPane.add(f, 1, 7);
        gridPane.add(dpng, 1, 8);
        gridPane.add(d1, 0, 9);
        gridPane.add(d2, 1, 9);
        gridPane.add(d3, 2, 9);
        // gridPane.add(removeLastButton, 0, 10);

        return gridPane;
    }

    private Button createMenuButton(ImageView image, String text) {
        Button button = new Button();
        button.setGraphic(image);
       
        button.setOnAction(event -> handleButtonClick(text));
        return button;
    }

    private void handleRemoveLastButtonClick() {
        // Find the last added item in the order
        String lastAddedItem = null;
        for (String item : itemCounts.keySet()) {
            if (itemCounts.get(item) > 0) {
                lastAddedItem = item;
            }
        }

        // If a last added item is found, decrease its quantity
        if (lastAddedItem != null) {
            itemCounts.put(lastAddedItem, itemCounts.get(lastAddedItem) - 1);
            updateTextAreaWithItemCounts();
        }
    }

    private void handleButtonClick(String buttonText) {
        itemCounts.put(buttonText, itemCounts.getOrDefault(buttonText, 0) + 1);
        updateTextAreaWithItemCounts();
    }

    private double handleButtonClick() {
        // Calculate the total price and prepare the output
        total = 0.0;
        ArrayList al = new ArrayList();
        StringBuilder orderSummary = new StringBuilder("Order Summary:\n");

        for (String item : itemCounts.keySet()) {
            int quantity = itemCounts.get(item);
            double price = getPriceForItem(item, quantity);
            total += price;
            if (quantity > 0) {
                orderSummary.append(item).append(" - Qty: ").append(quantity).append(" - Price: Rs.").append(price)
                        .append("\n");
            }
        }

        orderSummary.append("Total Price: Rs.").append(total);

        // Print the order summary to the terminal
       // System.out.println(orderSummary.toString());

        return total;
    }

    private void updateTextAreaWithItemCounts() {
        itemTextArea.clear();
        quantityTextArea.clear();
        priceTextArea.clear();
        totalArea.clear();

        for (String item : itemCounts.keySet()) {
            double price = getPriceForItem(item, itemCounts.get(item));

            itemTextArea.appendText(String.format("%-20s\n", item));
            quantityTextArea.appendText(String.format("%-5s\n", itemCounts.get(item)));
            priceTextArea.appendText(String.format("%-10s\n", price));
            totalArea.clear();
            totalArea.appendText(String.format("%-10s\n",handleButtonClick()   ));
        }

    }

    private double getPriceForItem(String item, int quantity) {

        if (item.contains("Coffee")) {
            // Pricing for coffee items
            double coffeePrice = 30.0; // Base price for coffee
            return coffeePrice * quantity; // Adjust the price based on quantity
        } else if (item.contains("Pizza")) {
            // Pricing for pizza items
            double pizzaPrice = 100.0; // Base price for pizza
            return pizzaPrice * quantity; // Adjust the price based on quantity
        } else if (item.contains("Burger")) {
            // Pricing for burger items
            double burgerPrice = 50.0; // Base price for burger
            return burgerPrice * quantity; // Adjust the price based on quantity
        } else if (item.contains("Sandwich")) {
            // Pricing for sandwich items
            double sandwichPrice = 80.0; // Base price for sandwich
            return sandwichPrice * quantity; // Adjust the price based on quantity
        } else if (item.contains("Fries")) {
            // Pricing for fries
            double friesPrice = 60.0; // Base price for fries
            return friesPrice * quantity; // Adjust the price based on quantity
        } else if (item=="Coca"||item=="Pepsi"||item=="Sprite") {
            // Pricing for drinks
            double drinkPrice = 20.0; // Base price for drinks
            return drinkPrice * quantity; // Adjust the price based on quantity
        } else {
            // Default price for unknown items
            return 0.0;
        }
    }

    private ImageView createRoundedCornerImageView(Image image, double width, double height) {
        ImageView imageView = new ImageView(image);
        imageView.setFitWidth(width);
        imageView.setFitHeight(height);

        // Create a rectangle with rounded corners
        Rectangle clip = new Rectangle(width, height);
        clip.setArcWidth(60); // Adjust the corner roundness as needed
        clip.setArcHeight(60);

        imageView.setClip(clip);

        return imageView;
    }

}
