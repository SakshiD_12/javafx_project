import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class CustomerDailyData extends Application {
    @Override
    public void start(Stage primaryStage) {
        StackPane root = new StackPane();
        root.setPrefSize(900.0, 700.0);

        AnchorPane anchorPane = new AnchorPane();
        anchorPane.setPrefSize(200.0, 200.0);

        ImageView imageView = new ImageView();
        imageView.setFitHeight(700.0);
        imageView.setFitWidth(900.0);
        imageView.setLayoutX(-2.0);
        imageView.setLayoutY(1.0);
        imageView.setPickOnBounds(true);
        imageView.setImage(new Image("cust.jpg"));

        TableView tableView = new TableView();
        tableView.setLayoutX(36.0);
        tableView.setLayoutY(155.0);
        tableView.setPrefHeight(487.0);
        tableView.setPrefWidth(827.0);

        TableColumn serialNoColumn = new TableColumn("Sr No.");
        serialNoColumn.setPrefWidth(75.0);
        serialNoColumn.setStyle("-fx-background-color: #B6FFFA;");

        TableColumn customerNameColumn = new TableColumn("Customer Name");
        customerNameColumn.setPrefWidth(178.4);
        customerNameColumn.setStyle("-fx-background-color: #B6FFFA;");

        TableColumn addressColumn = new TableColumn("Address");
        addressColumn.setPrefWidth(137.6);
        addressColumn.setStyle("-fx-background-color: #B6FFFA;");

        TableColumn mobNoColumn = new TableColumn("Mob No.");
        mobNoColumn.setPrefWidth(136.0);
        mobNoColumn.setStyle("-fx-background-color: #B6FFFA;");

        TableColumn dateColumn = new TableColumn("Date");
        dateColumn.setPrefWidth(114.4);
        dateColumn.setStyle("-fx-background-color: #B6FFFA;");

        TableColumn timeColumn = new TableColumn("Time");
        timeColumn.setPrefWidth(75.2);
        timeColumn.setStyle("-fx-background-color: #B6FFFA;");

        TableColumn totalBillColumn = new TableColumn("Total Bill");
        totalBillColumn.setPrefWidth(108.8);
        totalBillColumn.setStyle("-fx-background-color: #B6FFFA;");

        tableView.getColumns().addAll(serialNoColumn, customerNameColumn, addressColumn, mobNoColumn, dateColumn, timeColumn, totalBillColumn);

        Label label = new Label("Customers Data");
        label.setLayoutX(321.0);
        label.setLayoutY(38.0);
        label.setPrefHeight(71.0);
        label.setPrefWidth(239.0);
        label.setStyle("-fx-background-color: white;");
        label.setFont(Font.font("System Bold", 32.0));

        anchorPane.getChildren().addAll(imageView, tableView, label);
        root.getChildren().add(anchorPane);

        Scene scene = new Scene(root, 900, 700);
        primaryStage.setScene(scene);
        primaryStage.setTitle("JavaFX App");
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
