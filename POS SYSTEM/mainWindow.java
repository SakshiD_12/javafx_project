import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

public class mainWindow extends Application {

    LoginPage lp = new LoginPage();
    SignUP su=new SignUP();
    static AboutUs ab = new AboutUs();
    //static aboutus ab=new aboutus();
    //Firstwindow fw1=new Firstwindow();
    Stage primaryStage;

    StackPane stackPane;
    @Override
    public void start(Stage primaryStage) {
        // // Create an instance of AnotherClass
        // Firstwindow fw = new Firstwindow();

        // // Call the method from AnotherClass
        // fw.goToFirstwindow(primaryStage);

        /*StackPane*/ stackPane = new StackPane();

        // Load and set the background image
        Image backgroundImage = new Image("Firstwindowimg.jpg");
        ImageView backgroundImageView = new ImageView(backgroundImage);
        backgroundImageView.setFitHeight(950);
        backgroundImageView.setFitWidth(1900);
        stackPane.getChildren().add(backgroundImageView);

        // Create an AnchorPane to center the buttons
        AnchorPane anchorPane = new AnchorPane();

        // Create and position the buttons in the AnchorPane
        Button button1 = new Button("Sign Up");
        button1.setLayoutX(1200); // Set X position
        button1.setLayoutY(20); // Set Y position
        button1.setPrefWidth(150); // Set button width
        button1.setPrefHeight(50); // Set button height

        Button button2 = new Button("Login");
        button2.setLayoutX(1400); // Set X position
        button2.setLayoutY(20); // Set Y position
        button2.setPrefWidth(150); // Set button width
        button2.setPrefHeight(50); // Set button height

        Button button3 = new Button("About Us");
        button3.setLayoutX(1600); // Set X position
        button3.setLayoutY(20); // Set Y position
        button3.setPrefWidth(150); // Set button width
        button3.setPrefHeight(50); // Set button height

        // Apply styles using CSS
        button1.setStyle("-fx-font-weight: bold; -fx-background-radius: 28; -fx-background-color: #F8F1F1; -fx-font-family: 'Times New Roman'; -fx-font-size: 22px;");
        button2.setStyle("-fx-font-weight: bold; -fx-background-radius: 28; -fx-background-color: #F8F1F1; -fx-font-family: 'Times New Roman'; -fx-font-size: 22px;;");
        button3.setStyle("-fx-font-weight: bold; -fx-background-radius: 28; -fx-background-color: #F8F1F1; -fx-font-family: 'Times New Roman'; -fx-font-size: 22px;;");

        anchorPane.getChildren().addAll(button1, button2, button3);

        // Create a Text node for the coffee shop message (Welcome)
        Text coffeeShopText = new Text("Welcome To CUP AND CONVO");
        coffeeShopText.setStyle("-fx-font-size: 80px; -fx-fill: white; -fx-font-weight: bold; -fx-font-family: 'Times New Roman';");

        // Set layout for coffeeShopText (Welcome) at the top center
        AnchorPane.setTopAnchor(coffeeShopText, 100.0);
        AnchorPane.setLeftAnchor(coffeeShopText, 100.0);

        // Create a Text node for the rest of the coffee shop message
        Text coffeeShopText1 = new Text("This coffee shop runs on");
        coffeeShopText1.setStyle("-fx-font-size: 80px; -fx-fill: white; -fx-font-weight: bold; -fx-font-family: 'Times New Roman';");

        // Set layout for coffeeShopText1 (other text) at the bottom center
        AnchorPane.setBottomAnchor(coffeeShopText1, 400.0);
        AnchorPane.setLeftAnchor(coffeeShopText1, 120.0);

        Text coffeeShopText2 = new Text("love,laughter and a whole lot of");
        coffeeShopText2.setStyle("-fx-font-size: 80px; -fx-fill: white; -fx-font-weight: bold; -fx-font-family: 'Times New Roman';");

        // Set layout for coffeeShopText1 (other text) at the bottom center
        AnchorPane.setBottomAnchor(coffeeShopText2, 300.0);
        AnchorPane.setLeftAnchor(coffeeShopText2, 50.0);

        Text coffeeShopText3 = new Text("strong coffee");
        coffeeShopText3.setStyle("-fx-font-size: 80px; -fx-fill: white; -fx-font-weight: bold; -fx-font-family: 'Times New Roman';");

        // Set layout for coffeeShopText1 (other text) at the bottom center
        AnchorPane.setBottomAnchor(coffeeShopText3, 200.0);
        AnchorPane.setLeftAnchor(coffeeShopText3, 130.0);

        anchorPane.getChildren().addAll(coffeeShopText, coffeeShopText1,coffeeShopText2,coffeeShopText3);

        // Add the AnchorPane to the StackPane
        stackPane.getChildren().add(anchorPane);

        // Create the scene and set it to the primaryStage
        Scene scene = new Scene(stackPane, 1900, 950);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Core2Web");
        this.primaryStage = primaryStage;
        primaryStage.show();

        // Create a transition for the Welcome text
        TranslateTransition welcomeTransition = new TranslateTransition(Duration.seconds(2), coffeeShopText);
        welcomeTransition.setByY(200); // Move up by 1000 pixels (upside down to right-side up)
        welcomeTransition.play();

        // Create a transition for the This coffee shop runs on
        TranslateTransition translateTransition1 = new TranslateTransition(Duration.seconds(4), coffeeShopText1);
        translateTransition1.setFromX(-coffeeShopText1.getLayoutBounds().getWidth()); // Start offscreen to the left
        translateTransition1.setToX(3); // Move to the right

        // Create a transition for the love,laughter and a whole lot of
        TranslateTransition translateTransition2 = new TranslateTransition(Duration.seconds(4), coffeeShopText2);
        translateTransition2.setFromX(scene.getWidth()); // Start offscreen to the right
        translateTransition2.setToX(80); // Move to the left

        // Create a transition for the Strong coffee
        TranslateTransition translateTransition3 = new TranslateTransition(Duration.seconds(1), coffeeShopText3);
        translateTransition3.setFromX(30); // Start offscreen to the left
        translateTransition3.setToX(3); // Move to the right
    
    
        welcomeTransition.play();
        translateTransition1.play();
        translateTransition2.play();
        translateTransition3.play();

         button2.setOnAction(e -> lp.goToLoginPage(primaryStage));
        button1.setOnAction(e -> su. goToSignUpPage(primaryStage));
        button3.setOnAction(e->ab.goToAboutUs(primaryStage));

    

    }

    public static void main(String[] args) {
        launch(args);
    }
}
