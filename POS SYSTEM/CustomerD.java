
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class CustomerD{
    
    static LoginPage lp = new LoginPage();
    static MenuPage mp = new MenuPage();
    
    public void goToCustomerPage(Stage primaryStage) {
        StackPane root = new StackPane();

        AnchorPane anchorPane1 = new AnchorPane();
        anchorPane1.setPrefHeight(950.0);
        anchorPane1.setPrefWidth(1900.0);

        ImageView backgroundImageView = new ImageView();
        backgroundImageView.setFitHeight(970.0);
        backgroundImageView.setFitWidth(1900.0);
        backgroundImageView.setLayoutX(1.0);
        backgroundImageView.setLayoutY(-12.0);
        backgroundImageView.setOpacity(0.47);
        backgroundImageView.setPickOnBounds(true);
        Image backgroundImage = new Image("pizza3.jpg");
        backgroundImageView.setImage(backgroundImage);

        AnchorPane anchorPane2 = new AnchorPane();
        anchorPane2.setLayoutX(69.0);
        anchorPane2.setLayoutY(81.0);
        anchorPane2.setPrefHeight(800.0);
        anchorPane2.setPrefWidth(1750.0);
        anchorPane2.setStyle("-fx-background-color: F8FF95;");

        AnchorPane anchorPane3 = new AnchorPane();
        anchorPane3.setLayoutX(385.0);
        anchorPane3.setLayoutY(1.0);
        anchorPane3.setPrefHeight(584.0);
        anchorPane3.setPrefWidth(400.0);

        ImageView pizzaImageView = new ImageView();
        pizzaImageView.setFitHeight(840.0);
        pizzaImageView.setFitWidth(970);
        pizzaImageView.setLayoutX(400);
        pizzaImageView.setLayoutY(0.8);
        pizzaImageView.setPickOnBounds(true);
        Image pizzaImage = new Image("platter1.jpg");
        pizzaImageView.setImage(pizzaImage);

        Label titleLabel = new Label("Customer Details");
        titleLabel.setLayoutX(110.0);
        titleLabel.setLayoutY(62.0);
        titleLabel.setFont(Font.font("System Bold", 70.0));

        Label nameLabel = new Label("Name");
        nameLabel.setLayoutX(140.0);
        nameLabel.setLayoutY(222.0);
        nameLabel.setFont(Font.font(40.0));

        Label mobileLabel = new Label("Mob No.");
        mobileLabel.setLayoutX(140.0);
        mobileLabel.setLayoutY(300.0);
        mobileLabel.setFont(Font.font(40.0));

        Label addressLabel = new Label("Address");
        addressLabel.setLayoutX(140.0);
        addressLabel.setLayoutY(378.0);
        addressLabel.setFont(Font.font(40.0));

        // Date Label
        Label dateLabel = new Label("Date");
        dateLabel.setLayoutX(140.0);
        dateLabel.setLayoutY(456.0);
        dateLabel.setFont(Font.font(40.0));

        // Time Label
        Label timeLabel = new Label("Time");
        timeLabel.setLayoutX(140.0);
        timeLabel.setLayoutY(538.0);
        timeLabel.setFont(Font.font(40.0));

        TextField nameField = new TextField();
        nameField.setLayoutX(350.0);
        nameField.setLayoutY(230.0);
        nameField.setPrefHeight(40.0);
        nameField.setPrefWidth(230.0);
        nameField.setPromptText("Name");

        TextField mobileField = new TextField();
        mobileField.setLayoutX(350.0);
        mobileField.setLayoutY(305.0);
        mobileField.setPrefHeight(40.0);
        mobileField.setPrefWidth(230.0);
        mobileField.setPromptText("Mobile Number");

        TextField addressField = new TextField();
        addressField.setLayoutX(350.0);
        addressField.setLayoutY(380.0);
        addressField.setPrefHeight(40.0);
        addressField.setPrefWidth(230.0);
        addressField.setPromptText("Address");

        // Date Field
        DatePicker dateField = new DatePicker();
        dateField.setLayoutX(350.0);
        dateField.setLayoutY(460.0);
        dateField.setPrefHeight(40.0);
        dateField.setPrefWidth(230.0);

        // Time Field (Assuming a TextField for manual input, you can also use a TimePicker)
        TextField timeField = new TextField();
        timeField.setLayoutX(350.0);
        timeField.setLayoutY(542.0);
        timeField.setPrefHeight(40.0);
        timeField.setPrefWidth(230.0);
        timeField.setPromptText("Time");

        // Create a Button
        Button orderButton = new Button("Place Order");
        orderButton.setLayoutX(240.0);
        orderButton.setLayoutY(620.0);
        orderButton.setMnemonicParsing(false);
        orderButton.setPrefHeight(60.0);
        orderButton.setPrefWidth(240.0);
        orderButton.setFont(Font.font("System Bold", 30.0));
        orderButton.setStyle(" -fx-background-color: FF6969; -fx-text-fill: white;");

        Button backButton = new Button("Back");
        backButton.setLayoutX(590.0);
        backButton.setLayoutY(760.0);
        backButton.setMnemonicParsing(false);
        backButton.setPrefHeight(40.0);
        backButton.setPrefWidth(150.0);
        backButton.setFont(Font.font("System Bold", 25.0));
        backButton.setStyle("-fx-background-radius: 28;-fx-background-color: FF6969; -fx-text-fill: white;");
        

        //root.getChildren().add(anchorPane1);
        anchorPane2.getChildren().add(anchorPane3);
        anchorPane3.getChildren().add(pizzaImageView);
        anchorPane2.getChildren().addAll(titleLabel, nameLabel, mobileLabel, addressLabel, nameField, mobileField, addressField, orderButton,dateLabel,dateField,timeLabel,timeField,backButton);
        anchorPane1.getChildren().addAll(backgroundImageView, anchorPane2);
        //anchorPane2.getChildren().add(anchorPane3);
        root.getChildren().add(anchorPane1);

        Scene scene = new Scene(root, 1900, 950);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Pizza Order App");
        primaryStage.show();

        backButton.setOnAction(e->lp.goToLoginPage(primaryStage));

        //orderButton.setOnAction(e->mp.goToMenuPage(primaryStage));
        orderButton.setOnAction(e -> {
            String name = nameField.getText();
            String mobile = mobileField.getText();
            String address = addressField.getText();
            String orderDate = dateField.getValue().toString();
            String orderTime = timeField.getText();
            
            // Insert customer data into the database
            CustomerDataAccess.insertCustomerData(name, mobile, address, orderDate, orderTime);
        
            // Navigate to the menu page or perform any other actions you need.
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setContentText("Data Added succesfully");
            alert.showAndWait();
            //lp.goToLoginPage(primaryStage);
            mp.goToMenuPage(primaryStage);
        });
        
    }
}