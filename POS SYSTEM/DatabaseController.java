 import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class DatabaseController{

     // JDBC URL, username, and password of MySQL server
        String url = "jdbc:mysql://localhost:3306/coffeeshop";
        String user = "root";
        String password = "root";
        java.sql.Statement stmt;

    public void connection() {
    
        try {
            // Establish a connection to the database
            Connection connection = DriverManager.getConnection(url, user, password);
            stmt = connection.createStatement();
            
            System.out.println("Connected to the database");

        } catch (SQLException e) {
            System.err.println("Connection failed! Check the console for details.");
            e.printStackTrace();
        }
    }

    
    public ObservableList<String> getAdminName(){
    
        String query = "select username from adminlogin";
        ResultSet res;

        ObservableList<String> data = FXCollections.observableArrayList();
        
        try {
            res = stmt.executeQuery(query);
            while(res.next()){
            String str = new String(res.getString(1));
            data.add(str);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return data;
    }

    public ObservableList<String> getPass() {

        String query = "select password from adminlogin";
        ResultSet res;

        ObservableList<String> data = FXCollections.observableArrayList();
        
        try {
            res = stmt.executeQuery(query);
            while(res.next()){
            String str = new String(res.getString(1));
            data.add(str);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return data;
        
    }
}

    