
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.animation.TranslateTransition;

public class SignUP {

    PasswordField passwordField = new PasswordField();
     TextField usernameField = new TextField();
     Button SignUPButton;

    static LoginPage lp=new LoginPage();
    static mainWindow m = new mainWindow();
   // Firstwindow fp = new Firstwindow();

   static DatabaseConnection dbc=new DatabaseConnection();
     
    
    public void goToSignUpPage(Stage primaryStage) {
    
        StackPane root = new StackPane();

        AnchorPane anchorPane = new AnchorPane();
        anchorPane.setPrefSize(500, 400);
       // anchorPane.setStyle("-fx-background-color: #F5F7F8");

        AnchorPane innerAnchorPane = new AnchorPane();
        innerAnchorPane.setPrefSize(600, 400);

        ImageView imageView = new ImageView(new Image("17.jpg"));
        imageView.setFitWidth(1200);
        imageView.setFitHeight(955);
        imageView.setLayoutX(-1);
        imageView.setLayoutY(-4);
        imageView.setPickOnBounds(true);

        ImageView secondImage = new ImageView(new Image("17.jpg"));
        secondImage.setFitWidth(750);
        secondImage.setFitHeight(955);
        secondImage.setLayoutX(1200);
        secondImage.setLayoutY(-4);
        secondImage.setOpacity(0.3); // Adjust opacity as needed

       // TextField usernameField = new TextField();
        usernameField.setLayoutX(1370);
        usernameField.setLayoutY(400);
        usernameField.setPrefWidth(400);
        usernameField.setPrefHeight(50);
        usernameField.setPromptText("USERNAME");
        usernameField.setStyle("-fx-background-radius: 28;");

       // PasswordField passwordField = new PasswordField();
        passwordField.setLayoutX(1370);
        passwordField.setLayoutY(490);
        passwordField.setPrefWidth(400);
        passwordField.setPrefHeight(50);
        passwordField.setPromptText("PASSWORD");
        passwordField.setStyle("-fx-background-radius: 28;");

        SignUPButton = new Button("SIGN UP");
        SignUPButton.setLayoutX(1370);
        SignUPButton.setLayoutY(630);
        SignUPButton.setPrefWidth(400);
        SignUPButton.setPrefHeight(50);
        SignUPButton.setStyle("-fx-font-weight: bold; -fx-font-size: 25px; -fx-background-radius: 28;");
        SignUPButton.setMnemonicParsing(false);
        SignUPButton.setTextFill(Color.WHITE);


        SignUPButton.getStyleClass().add("button-login");
        SignUPButton.getStyleClass().add("button-login:hover");

        Label titleLabel = new Label("Create Account");
        titleLabel.setLayoutX(1300);
        titleLabel.setLayoutY(-10); // Start off the screen above
        titleLabel.setPrefWidth(500);
        titleLabel.setPrefHeight(90);
        titleLabel.setFont(new Font(55.0));
        titleLabel.setStyle("-fx-font-family: 'Times New Roman';");

        titleLabel.getStyleClass().add("label-title");


        // Forgot Password link
        Hyperlink forgotPasswordLink = new Hyperlink("Already have account?");
        forgotPasswordLink.setLayoutX(1370);
        forgotPasswordLink.setLayoutY(560);
        forgotPasswordLink.setTextFill(Color.BLACK);
        forgotPasswordLink.setFont(new Font(25));


        Button backButton = new Button("Back");
        backButton.setLayoutX(1800);
        backButton.setLayoutY(800);
        backButton.setPrefSize(100, 40);
        backButton.setStyle(
            "-fx-background-color: black; " + // Button background color
            "-fx-text-fill: white; " + // Button text color
            "-fx-font-size: 20px; " + // Button text size
            "-fx-border-radius: 50px;" +
            "-fx-background-radius: 28;" // Rounded button
        );

        innerAnchorPane.getChildren().addAll(imageView, secondImage,usernameField, passwordField, SignUPButton, titleLabel,backButton,forgotPasswordLink);
        anchorPane.getChildren().add(innerAnchorPane);
        root.getChildren().add(anchorPane);

        Scene scene = new Scene(root, 1900, 950);
        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.setResizable(false);
        scene.getStylesheets().add("Styles.css");

        // Create a transition for the "Login Account" label
        TranslateTransition labelTransition = new TranslateTransition(Duration.seconds(2), titleLabel);
        labelTransition.setToY(250); // Move it to its original position

        // Play the label transition
        labelTransition.play();

        backButton.setOnAction(e ->m.start(primaryStage));
        forgotPasswordLink.setOnAction(e->lp.goToLoginPage(primaryStage));

        SignUPButton.setOnAction(e->dbc.insertUser(usernameField.getText(),passwordField.getText(),primaryStage));
        
    }
}
